<?php

namespace app\models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Ticket extends \Spot\Entity
{
    protected static $table = 'tickets';
    public static function fields()
    {
        return [
            'id'            => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'name'          => ['type' => 'string', 'required' => true],
            'description'   => ['type' => 'text', 'required' => true],
            'status_id'     => ['type' => 'integer', 'required' => true],
            'type_id'       => ['type' => 'integer', 'required' => true],
            'reporter_id'   => ['type' => 'integer', 'required' => true],
            'assignee_id'   => ['type' => 'integer', 'required' => true],
            'create_date'   => ['type' => 'datetime', 'value' => new \DateTime()],
            'update_date'   => ['type' => 'datetime', 'value' => new \DateTime()],
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'status' => $mapper->belongsTo($entity, 'app\models\Status', 'status_id'),
            'type' => $mapper->belongsTo($entity, 'app\models\Type', 'type_id'),
            'assignee' => $mapper->belongsTo($entity, 'app\models\User', 'reporter_id'),
            'reporter' => $mapper->belongsTo($entity, 'app\models\User', 'assignee_id'),
            'comments' => $mapper->hasMany($entity, 'app\models\Comment', 'ticket_id')->order(['create_date' => 'ASC']),
        ];
    }
}