<?php

namespace app\models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Comment extends \Spot\Entity
{
    protected static $table = 'comments';
    public static function fields()
    {
        return [
            'id'            => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'ticket_id'     => ['type' => 'integer', 'required' => true],
            'user_id'       => ['type' => 'integer', 'required' => true],
            'text'          => ['type' => 'text', 'required' => true],
            'create_date'   => ['type' => 'datetime', 'value' => new \DateTime()],
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'author' => $mapper->belongsTo($entity, 'app\models\User', 'user_id'),
            'ticket' => $mapper->belongsTo($entity, 'app\models\Ticket', 'ticket_id'),
        ];
    }
}