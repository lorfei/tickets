<?php

namespace app\models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Status extends \Spot\Entity
{
    protected static $table = 'statuses';
    public static function fields()
    {
        return [
            'id'            => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'name'          => ['type' => 'string', 'required' => true],
            'description'   => ['type' => 'text', 'required' => true],
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'tickets' => $mapper->hasMany($entity, 'app\models\Ticket', 'status_id')->order(['update_date' => 'ASC']),
        ];
    }
}