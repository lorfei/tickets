<?php

namespace app\models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class User extends \Spot\Entity
{
    protected static $table = 'users';
    public static function fields()
    {
        return [
            'id'        => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'login'     => ['type' => 'string', 'required' => true],
            'password'  => ['type' => 'string', 'required' => true],
            'role'      => ['type' => 'string', 'default' => 'user'],
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'reporter' => $mapper->belongsTo($entity, 'app\models\Ticket', 'reporter_id'),
            'assignee' => $mapper->belongsTo($entity, 'app\models\Ticket', 'assignee_id'),
        ];
    }
}