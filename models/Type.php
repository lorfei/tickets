<?php

namespace app\models;

use Spot\EntityInterface as Entity;
use Spot\MapperInterface as Mapper;

class Type extends \Spot\Entity
{
    protected static $table = 'types';
    public static function fields()
    {
        return [
            'id'            => ['type' => 'integer', 'primary' => true, 'autoincrement' => true],
            'name'          => ['type' => 'string', 'required' => true],
            'description'   => ['type' => 'text', 'required' => true],
        ];
    }

    public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'tickets' => $mapper->hasMany($entity, 'app\models\Ticket', 'type_id')->order(['update_date' => 'ASC']),
        ];
    }
}