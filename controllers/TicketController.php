<?php

namespace app\controllers;

use app\framework\Controller as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;

class TicketController extends Controller {

    public function getIndex( Request $request, Response $response, $args) {
        $locator = $this->getDbLocator();
        $mapper = $locator->mapper('app\models\Ticket');

        $tickets = $mapper->all();
        
        return $this->render('home', ['tickets' => $tickets]);

    }

    public function getCreateTicket( Request $request, Response $response, $args) {
        $session = new Session();

        if ($session->get('id',0)) {
            $locator = $this->getDbLocator();
            $type = $locator->mapper('app\models\Type');
            $types = $type->all();
            $user = $locator->mapper('app\models\User');
            $users = $user->all();
            return $this->render('create_ticket', ['types' => $types, 'users' => $users]);
        } else {
            return $this->render('404');
        }
    }

    public function postCreateTicket( Request $request, Response $response, $args) {
        $session = new Session();

        if ($session->get('id',0)) {
            $errors = [];

            try {
                $NameValidator = v::length(3,100);
                $NameValidator->assert($request->get('name'));
            } catch(NestedValidationException $exception) {
                $errors['name'] = (array)$exception->findMessages([
                    'length' => 'Name must not have more than 3 chars and less then 100',
                ]);
            }

            try {
                $TypeValidator = v::length(5,null);
                $TypeValidator->assert($request->get('description'));
            } catch(NestedValidationException $exception) {
                $errors['description'] = (array)$exception->findMessages([
                    'length' => 'Description must not have more than 5 chars',
                ]);
            }

            $locator = $this->getDbLocator();
            $type = $locator->mapper('app\models\Type');
            $types = $type->all();
            $types_array = [];
            

            foreach ($types as $type) {
                $types_array[] = $type->id;
            }

            try {
                $TypeValidator = v::in($types_array);
                $TypeValidator->assert($request->get('type'));
            } catch(NestedValidationException $exception) {
                $errors['type'] = (array)$exception->findMessages([
                    'in' => 'Incorrect type',
                ]);
            }

            $user = $locator->mapper('app\models\User');
            $users = $user->all();
            $users_array = [];

            foreach ($users as $user) {
                $users_array[] = $user->id;
            }

            try {
                $UserValidator = v::in($users_array);
                $UserValidator->assert($request->get('assignee'));
            } catch(NestedValidationException $exception) {
                $errors['assignee'] = (array)$exception->findMessages([
                    'in' => 'Incorrect user',
                ]);
            }

            if (!empty($errors)) {
                return $this->render('create_ticket', ['types' => $types, 'users' => $users, 'errors' => $errors]);
            }

            $tickets = $locator->mapper('app\models\Ticket');

            $ticket = $tickets->insert([
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'status_id' => '1',
                'type_id' => $request->get('type'),
                'reporter_id' => $session->get('id'),
                'assignee_id' => $request->get('assignee'),
            ]);

            return $this->render('create_ticket', ['types' => $types, 'users' => $users, 'ticket' => $ticket]);
        } else {
            return $this->render('404');
        }
    }


    public function getUpdateTicket( Request $request, Response $response, $args) {
        $session = new Session();

        if ($session->get('id',0)) {
            $locator = $this->getDbLocator();
            $tickets = $locator->mapper('app\models\Ticket');
            $type = $locator->mapper('app\models\Type');
            $types = $type->all();
            $user = $locator->mapper('app\models\User');
            $users = $user->all();
            $status = $locator->mapper('app\models\Status');
            $statuses = $status->all();

            if (!is_numeric($args['id'])) {
                return $this->render('404');
            }

            $ticket = $tickets->where(['id' => $args['id']])->first();

            return $this->render('update_ticket', ['types' => $types, 'users' => $users, 'statuses' => $statuses, 'ticket' => $ticket]);
        } else {
            return $this->render('404');
        }
    }

    public function postUpdateTicket( Request $request, Response $response, $args) {
        $session = new Session();

        if ($session->get('id',0)) {
            $errors = [];

            try {
                $NameValidator = v::length(3,100);
                $NameValidator->assert($request->get('name'));
            } catch(NestedValidationException $exception) {
                $errors['name'] = (array)$exception->findMessages([
                    'length' => 'Name must not have more than 3 chars and less then 100',
                ]);
            }

            try {
                $TypeValidator = v::length(5,null);
                $TypeValidator->assert($request->get('description'));
            } catch(NestedValidationException $exception) {
                $errors['description'] = (array)$exception->findMessages([
                    'length' => 'Description must not have more than 5 chars',
                ]);
            }

            $locator = $this->getDbLocator();
            $type = $locator->mapper('app\models\Type');
            $types = $type->all();
            $types_array = [];
            

            foreach ($types as $type) {
                $types_array[] = $type->id;
            }

            try {
                $TypeValidator = v::in($types_array);
                $TypeValidator->assert($request->get('type'));
            } catch(NestedValidationException $exception) {
                $errors['type'] = (array)$exception->findMessages([
                    'in' => 'Incorrect type',
                ]);
            }

            $status = $locator->mapper('app\models\Status');
            $statuses = $status->all();
            $statuses_array = [];
            

            foreach ($statuses as $status) {
                $statuses_array[] = $status->id;
            }

            try {
                $StatusValidator = v::in($statuses_array);
                $StatusValidator->assert($request->get('status'));
            } catch(NestedValidationException $exception) {
                $errors['status'] = (array)$exception->findMessages([
                    'in' => 'Incorrect status',
                ]);
            }

            $user = $locator->mapper('app\models\User');
            $users = $user->all();
            $users_array = [];

            foreach ($users as $user) {
                $users_array[] = $user->id;
            }

            try {
                $UserValidator = v::in($users_array);
                $UserValidator->assert($request->get('assignee'));
            } catch(NestedValidationException $exception) {
                $errors['assignee'] = (array)$exception->findMessages([
                    'in' => 'Incorrect user',
                ]);
            }
            
            $tickets = $locator->mapper('app\models\Ticket');

            $ticket = $tickets->where(['id' => $args['id']])->first();

            if (!empty($errors)) {
                return $this->render('update_ticket', ['types' => $types, 'users' => $users, 'statuses' => $statuses, 'errors' => $errors, 'ticket' => $ticket]);
            }

            $ticket->name = $request->get('name');
            $ticket->description = $request->get('description');
            $ticket->status_id = $request->get('status');
            $ticket->type_id = $request->get('type');
            $ticket->assignee_id = $request->get('assignee');
            $tickets->update($ticket);

            return $this->render('update_ticket', ['types' => $types, 'users' => $users, 'statuses' => $statuses, 'ticket' => $ticket]);
        } else {
            return $this->render('404');
        }
    }

    public function getTicket( Request $request, Response $response, $args) {
        $locator = $this->getDbLocator();
        $tickets = $locator->mapper('app\models\Ticket');
        $comments = $locator->mapper('app\models\Comment');

        if (!is_numeric($args['id'])) {
            return $this->render('404');
        }

        $ticket = $tickets->where(['id' => $args['id']])->with('comments')->first();
        $comments = $comments->where(['ticket_id' => $args['id']]);

        if ($ticket) {
            return $this->render('ticket', ['ticket' => $ticket, 'comments' => $comments]);
        }

        return $this->render('404');
    }

    public function postAddComment( Request $request, Response $response, $args) {
        $session = new Session();

        if ($session->get('id',0)) {


            $locator = $this->getDbLocator();
            $tickets = $locator->mapper('app\models\Ticket');
            $comments = $locator->mapper('app\models\Comment');
            $errors = [];

            if (!is_numeric($args['id'])) {
                return $this->render('404');
            }

            try {
                $CommentValidator = v::length(5,null);
                $CommentValidator->assert($request->get('comment'));
            } catch(NestedValidationException $exception) {
                $errors['comment'] = (array)$exception->findMessages([
                    'length' => 'Comment must not have more than 5 chars',
                ]);
            }

            if (!empty($errors)) {

                $ticket = $tickets->where(['id' => $args['id']])->with('comments')->first();
                $comments = $comments->where(['ticket_id' => $args['id']]);

                return $this->render('ticket', ['ticket' => $ticket, 'comments' => $comments, 'errors' => $errors]);
            }

            $ticket = $tickets->where(['id' => $args['id']])->with('comments')->first();

            if ($ticket) {
                
                
                $comment = $comments->insert([
                    'ticket_id' => $args['id'],
                    'user_id' => $session->get('id'),
                    'text' => $request->get('comment'),
                ]);

                $comments = $comments->where(['ticket_id' => $args['id']]);

                return $this->render('ticket', ['ticket' => $ticket, 'comments' => $comments]);
            }
            return $this->render('404');
        } else {
            return $this->render('404');
        }
    }
}