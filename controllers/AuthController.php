<?php

namespace app\controllers;

use app\framework\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Respect\Validation\Validator as v;
use Respect\Validation\Exceptions\NestedValidationException;
use Symfony\Component\HttpFoundation\Session\Session;


class AuthController extends Controller {

    public function getLoginForm( Request $request, Response $response, $args) {
        return $this->render('login_form');
    }

    public function postLoginForm( Request $request, Response $response, $args) {
        $locator = $this->getDbLocator();
        $users = $locator->mapper('app\models\User');
        $errors = [];
        $user = $users->first(['login' => $request->get('login')]);

        if ($user) {
            if (password_verify($request->get('password'),$user->password)) {
                $session = new Session();
                $session->set('id', $user->id);
                $session->set('login', $user->login);
                $session->set('role', $user->role);
                return $this->render('login_end');
            } else {
                $errors['password'] = 'Wrong Password';
                return $this->render('login_form', ['error' => $errors]);                
            }
        } else {
            $errors['login'] = 'Such Login dont exist';
            return $this->render('login_form', ['error' => $errors]);
        }
    }

    public function getRegisterForm( Request $request, Response $response, $args) {
        return $this->render('register_form');
    }

    public function postRegisterForm( Request $request, Response $response, $args) {

        $locator = $this->getDbLocator();
        $users = $locator->mapper('app\models\User');
        $errors = [];

        try {
            $LoginValidator = v::length(3,100)->alnum();
            $LoginValidator->assert($request->get('login'));
        } catch(NestedValidationException $exception) {
            $errors['login'] = (array)$exception->findMessages([
                'alnum' => 'Login must contain only letters and digits',
                'length' => 'Login must not have more than 3 chars and less then 100',
            ]);
        }

        try {
            $PasswordValidator = v::length(6,100);
            $PasswordValidator->assert($request->get('password'));
        } catch(NestedValidationException $exception) {
            $errors['password'] = (array)$exception->findMessages([
                'length' => 'Password must not have more than 6 chars and less then 100',
            ]);
        }

        $user = $users->first(['login' => $request->get('login')]);

        if ($user) {
            $errors['exist'] = 'User with same login already exist!';
        }

        if (!empty($errors)) {
            return $this->render('register_form', ['errors' => $errors]);
        }
        
        $user = $users->insert([
            'login' => $request->get('login'),
            'password' => password_hash($request->get('password'), PASSWORD_DEFAULT),
        ]);

        return $this->render('register_end');
    }
}