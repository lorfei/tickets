<?php

namespace app\framework;

use \app\Base as base;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


abstract class Controller
{

    private static $viewsFolder = 'views';
    private static $db;
    public $layout = "/layout/home";

    public function __construct()
    {
        Sec::csrfCheck();

        if (method_exists($this, 'beforeAction')) {
            $this->beforeAction();
        }
    }

    public function __call($method, $args)
    {
        echo "The called action does not exist!";

        return $args[1];
    }

    public function render( $view, $data = [], $responseCode = 200)
    {
        $views_path = realpath(dirname(__DIR__) . DIRECTORY_SEPARATOR . self::$viewsFolder . DIRECTORY_SEPARATOR);
        $loader = new \Twig_Loader_Filesystem( $views_path);
        $twig = new \Twig_Environment($loader, ['debug' => true]);
        $twig->addExtension(new \Twig_Extension_Debug());

        $session = new Session();

        $data['user'] = [
            'id' => $session->get('id'),
            'login' => $session->get('login'),
            'role' => $session->get('role'),
        ];

        $data['csrf'] = Sec::getCsrf();



        $content = $twig->render( $view . '.html', $data);

        $response = new Response();

        $response->setContent($content);
        $response->setStatusCode($responseCode);

        return $response;
    }

    public static function getDbLocator()
    {
        if (is_null(self::$db)) {
            $config = \Spyc::YAMLLoad(dirname(__DIR__) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.yml');

            $cfg = new \Spot\Config();

            $cfg->addConnection('mysql', [
                'dbname' => $config['db_database'],
                'user' => $config['db_user'],
                'password' => $config['db_password'],
                'host' => $config['db_host'],
                'driver' => 'pdo_mysql',
            ]);

            $spot = new \Spot\Locator($cfg);

            self::$db = $spot;
        }

        return self::$db;
    }
}