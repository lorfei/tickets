<?php

namespace app\framework;

class Sec
{

    protected static $checked = false;

    public static function csrfCheck()
    {
        self::getCsrf();
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['csrf'])) {
                if ($_POST['csrf'] != $_SESSION[ 'csrf' ]) {
                    throw new \Exception("Csrf Error!", 403);
                }
            }
            else {
                throw new \Exception("Csrf Error!", 403);
            }
        }
    }

    public static function getCsrf()
    {
        if (!isset($_SESSION[ 'csrf' ])) {
            $token = base64_encode( time() . self::randomString( 32 ) );
            $_SESSION[ 'csrf' ] = $token;
        }

        return $_SESSION[ 'csrf' ];
    }

    protected static function randomString( $length )
    {
        $seed = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijqlmnopqrtsuvwxyz0123456789';
        $max = strlen( $seed ) - 1;
        $string = '';
        for ( $i = 0; $i < $length; ++$i )
            $string .= $seed{intval( mt_rand( 0.0, $max ) )};
        return $string;
    }
}