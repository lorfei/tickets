<?php
/**
 * Created by PhpStorm.
 * User: Amir Hossein Babaeian
 * Date: 12/26/15
 * Time: 10:44 AM
 */
use Symfony\Component\HttpFoundation\Request;

$router = new League\Route\RouteCollection;

$router->addRoute('GET', '/', 'app\controllers\TicketController::getIndex');

$router->addRoute('GET', '/ticket/create', 'app\controllers\TicketController::getCreateTicket');
$router->addRoute('POST', '/ticket/create', 'app\controllers\TicketController::postCreateTicket');

$router->addRoute('GET', '/ticket/update/{id}', 'app\controllers\TicketController::getUpdateTicket');
$router->addRoute('POST', '/ticket/update/{id}', 'app\controllers\TicketController::postUpdateTicket');

$router->addRoute('GET', '/ticket/{id}', 'app\controllers\TicketController::getTicket');
$router->addRoute('POST', '/ticket/{id}', 'app\controllers\TicketController::postAddComment');

$router->addRoute('GET', '/login', 'app\controllers\AuthController::getLoginForm');
$router->addRoute('POST', '/login', 'app\controllers\AuthController::postLoginForm');

$router->addRoute('GET', '/login/register', 'app\controllers\AuthController::getRegisterForm');
$router->addRoute('POST', '/login/register', 'app\controllers\AuthController::postRegisterForm');

$dispatcher = $router->getDispatcher();

$request = Request::createFromGlobals();
$response = $dispatcher->dispatch($request->getMethod(), $request->getPathInfo());

$response->send();
